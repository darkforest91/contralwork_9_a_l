@extends('layouts.app')

@section('content')

    <h2>@lang('Edit Comment')</h2>

    <form method="post" action="{{route('comments.update', ['comment' => $comment])}}" class="w-25 mt-5 mb-5">
        @csrf
        @method('put')
        <input type="hidden" id="photo_id" name="photo_id" value="{{$comment->photo->id}}">

        <div class="form-group">
            <label for="body">@lang('Comment'):</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3">{{$comment->body}}</textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="rating">@lang('Rating')</label>
            <select class="form-control @error('body') is-invalid @enderror" id="rating" name="rating">
                <option @if($comment->rating === '1') selected @endif>1</option>
                <option @if($comment->rating === '2') selected @endif>2</option>
                <option @if($comment->rating === '3') selected @endif>3</option>
                <option @if($comment->rating === '4') selected @endif>4</option>
                <option @if($comment->rating === '5') selected @endif>5</option>
            </select>
            @error('rating')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">@lang('Edit comment')</button>
    </form>

    <div class="mt-3 mb-5">
        <a href="{{route('photos.show', ['photo' => $comment->photo])}}">@lang('Back')</a>
    </div>

@endsection
