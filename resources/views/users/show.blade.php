@extends('layouts.app')

@section('content')

    <h2>@lang('Photos'):</h2>
    @can('view', $user)
        <a href="{{route('photos.create')}}">@lang('Create new photo')</a>
    @endcan

    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">@lang('Photo')</th>
            <th scope="col">@lang('Title')</th>
            <th scope="col" colspan="2">@lang('Actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($user->photos as $photo)
            <tr>
                @if($photo->picture == 'no_picture')
                    <td>
                        <a href="{{route('photos.show', ['photo' => $photo])}}">
                            <img src="{{asset('img/no_img.jpg')}}" alt="no_img" style="width: 100px; height: 100px">
                        </a>
                    </td>
                @else
                    <td>
                        <a href="{{route('photos.show', ['photo' => $photo])}}">
                            <img src="{{asset('storage/' . $photo->picture)}}" alt="{{$photo->picture}}" style="width: 100px; height: 100px">
                        </a>
                    </td>
                @endif
                <td>{{$photo->title}}</td>
                <td>
                    @can('update', $photo)
                        <a href="{{route('photos.edit', ['photo' => $photo])}}">@lang('Edit')</a>
                    @endcan
                </td>
                <td>
                    @can('delete', $photo)
                        <form method="post" action="{{route('photos.destroy', ['photo' => $photo])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger btn-sm">@lang('Remove')</button>
                        </form>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
