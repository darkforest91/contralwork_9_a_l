@extends('layouts.app')

@section('content')

    <h1>@lang('Create photo')</h1>

    <form method="post" action="{{route('photos.store')}}" class="w-50" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="title">@lang('Caption')</label>
            <input class="form-control @error('title') is-invalid @enderror" id="title" name="title">
            @error('title')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="picture">@lang('Photo')</label>
            <input type="file" class="form-control-file @error('picture') is-invalid @enderror" id="picture" name="picture">
            @error('picture')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">@lang('Create')</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('photos.index')}}">@lang('Back')</a>
    </div>

@endsection
