@extends('layouts.app')

@section('content')

    <h1>@lang('Edit photo')</h1>

    <form method="post" action="{{route('photos.update', ['photo' => $photo])}}" class="w-50" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="title">@lang('Caption')</label>
            <input class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{$photo->title}}">
            @error('title')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        @if($photo->picture == 'no_picture')
            <img src="{{asset('img/no_img.jpg')}}" alt="no_img" style="width:100px;height:100px;">
        @else
            <img src="{{asset('storage/' . $photo->picture)}}" alt="{{$photo->picture}}" style="width:100px;height:100px;"><br/><br>
        @endif

        <div class="form-group">
            <label for="picture">@lang('Photo')</label>
            <input type="file" class="form-control-file @error('picture') is-invalid @enderror" id="picture" name="picture" value="{{$photo->picture}}">
            @error('picture')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">@lang('Edit')</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('photos.index')}}">@lang('Back')</a>
    </div>

@endsection
