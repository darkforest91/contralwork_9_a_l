@extends('layouts.app')

@section('content')

    <a href="{{route('photos.create')}}">@lang('Create new photo')</a>

    <div class="row row-cols-1 row-cols-md-5 mt-4">
        @foreach($photos as $photo)
            <div class="card mb-5 mr-3" style="">
                @if($photo->picture == 'no_picture')
                    <a href="{{route('photos.show', ['photo' => $photo])}}">
                        <img src="{{asset('img/no_img.jpg')}}" alt="no_img" class="card-img-top">
                    </a>
                @else
                    <a href="{{route('photos.show', ['photo' => $photo])}}">
                        <img src="{{asset('storage/' . $photo->picture)}}" alt="{{$photo->picture}}" class="card-img-top"
                             style="width: 226px; height: 227px">
                    </a>
                @endif
                    <div class="card-body">
                        <p>{{$photo->title}}</p>
                        <p>@lang('Author'): <a href="{{route('users.show', ['user' => $photo->user])}}">{{$photo->user->name}}</a></p>
                    </div>

            </div>
        @endforeach
    </div>

    <div class="row p-5">
        <div class="col-12">
            {{ $photos->links() }}
        </div>
    </div>

@endsection
