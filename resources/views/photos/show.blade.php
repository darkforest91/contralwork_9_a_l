@extends('layouts.app')

@section('content')

    <h2>@lang('Author'): {{$photo->user->name}}</h2>

    @if($photo->picture == 'no_picture')
        <img src="{{asset('img/no_img.jpg')}}" alt="no_img">
    @else
        <img src="{{asset('storage/' . $photo->picture)}}" alt="{{$photo->picture}}">
    @endif

    <div>
        @can('update', $photo)
            <a href="{{route('photos.edit', ['photo' => $photo])}}">@lang('Edit')</a>
        @endcan
        @can('delete', $photo)
            <form method="post" action="{{route('photos.destroy', ['photo' => $photo])}}">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-outline-danger btn-sm">@lang('Remove')</button>
            </form>
        @endcan
    </div>

    <p>@lang('Average score'): {{$average_score}}</p>

    <div>
        <button class="show-comments-btn btn btn-info mt-4" type="button" data-toggle="collapse"
                data-target="#comments-block">@lang('Comments'): <span class="comments-number">{{$comments_count}}</span></button>
        <div id="comments-block" class=" collapse multi-collapse mt-5">
            @foreach($photo->comments as $comment)
                    <div class="card mb-3 w-50">
                        <h5 class="card-header">{{$comment->user->name}}, @lang('Score'): {{$comment->rating}}</h5>
                        <div class="card-body">
                            <p class="card-text">{{$comment->body}}</p>
                            <p class="card-text text-right"><small class="text-muted">{{$comment->created_at->diffForHumans()}}</small></p>
                            <div>
                                @can('update', $comment)
                                    <hr>
                                    <p><a href="{{route('comments.edit', ['comment' => $comment])}}">@lang('Edit comment')</a></p>
                                @endcan
                                @can('delete', $comment)
                                    <form class="mt-3" method="post" action="{{route('comments.destroy', ['comment' => $comment])}}">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-outline-danger btn-sm">@lang('Remove')</button>
                                    </form>
                                @endcan
                            </div>
                        </div>
                    </div>
            @endforeach
        </div>
    </div>

        <form method="post" action="{{route('comments.store')}}" class="w-25 mt-5 mb-5">
            @csrf
            <input type="hidden" id="photo_id" name="photo_id" value="{{$photo->id}}">

            <div class="form-group">
                <label for="body">@lang('Comment'):</label>
                <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3"></textarea>
                @error('body')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="rating">@lang('Rating')</label>
                <select class="form-control @error('body') is-invalid @enderror" id="rating" name="rating">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                @error('rating')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-success">@lang('Add comment')</button>
        </form>

    <div class="mt-3 mb-3">
        <a href="{{route('photos.index')}}">@lang('Back')</a>
    </div>

@endsection
