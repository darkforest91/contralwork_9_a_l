<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Photo;

class PhotosController extends Controller
{
    /**
     * PhotosController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $photos = Photo::paginate(12);
        return view('photos.index', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('photos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PhotoRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PhotoRequest $request)
    {
        $photo = new Photo($request->all());
        $photo->user_id = $request->user()->id;
        $file = $request->file('picture');
        $photo['picture'] = $file->store('photos', 'public');
        $photo->save();
        return redirect(route('photos.index'))->with('status', 'You have success added photo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id);
        $comments_count = $photo->comments->count();
        $average_score = number_format($photo->comments->avg('rating'), 1);
        return view('photos.show', compact('photo', 'comments_count', 'average_score'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $photo = Photo::findOrFail($id);
        return view('photos.edit', compact('photo'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param PhotoRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(PhotoRequest $request, $id)
    {
        $photo = Photo::findOrFail($id);
        $this->authorize('update', $photo);
        $data = $request->all();
        $file = $request->file('picture');
        $data['picture'] = $file->store('photos', 'public');
        $photo->update($data);

        return redirect(route('photos.index'))->with('status', 'You have success edited photo');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        $this->authorize('delete', $photo);
        $photo->delete();
        return redirect(route('photos.index'))->with('status', 'You have success removed photo');
    }
}
