<?php

namespace App\Orchid\Layouts;

use App\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('user_id', 'Comments author')
                ->sort()
                ->render(function (Comment $comment) {
                    return "{$comment->user->name}";
                }),
            TD::set('show', 'Show comment')
                ->render(function (Comment $comment) {
                    return Link::make()->icon('icon-envelope')->route('platform.comments.edit', $comment);
                }),
            TD::set('rating', 'Rating')->sort(),
            TD::set('created_at', 'Created')->sort(),
            TD::set('updated_at', 'Updated')->sort()
        ];
    }
}
