<?php

namespace App\Orchid\Screens;

use App\Comment;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create comments';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if ($this->exists) {
            $this->name = "Edit comment";
        }
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create Comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Edit comment')
                ->icon('icon-wrench')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove comment')
                ->icon('icon-trash')
                ->method('remove')->
                canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([

                Input::make('comment.user.name')
                    ->title('Name')
                    ->required(),
                TextArea::make('comment.body')
                    ->rows(10)
                    ->title('Comment')
                    ->required(),
                Select::make('comment.rating')
                    ->title('Rating')
                    ->options([
                        '1' => 1,
                        '2' => 2,
                        '3' => 3,
                        '4' => 4,
                        '5' => 5,
                    ])
                    ->required()
            ])
        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createOrUpdate(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have success edit comment');
        return redirect(route('platform.comments.list'));
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('Yuo have success delete comment')
            : Alert::warning('Comment is not deleted');
        return redirect(route('platform.comments.list'));
    }
}
