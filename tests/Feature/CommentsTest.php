<?php

namespace Tests\Feature;

use App\Comment;
use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentsTest extends TestCase
{
    use RefreshDatabase;

    private $user, $photo, $comments;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->photo = factory(Photo::class)->create([
            'user_id' => $this->user->id
        ]);
        $this->comments = factory(Comment::class, 5)->create([
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id
        ]);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateComment()
    {
        $this->actingAs($this->user);
        $comment = [
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id,
            'body' => 'Test Body',
            'rating' => 5
        ];
        $response = $this->post(route('comments.store', $comment));
        $response->assertRedirect(route('photos.show', ['photo' => $this->photo]));
        $this->assertDatabaseHas('comments', $comment);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedCreateCommentIfNotAuth()
    {
        $comment = [
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id,
            'body' => 'Test Body',
            'rating' => 5
        ];
        $response = $this->post(route('comments.store', $comment));
        $response->assertRedirect(route('login'));
        $this->assertDatabaseMissing('comments', $comment);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testSuccessUpdateComment()
    {
        $this->actingAs($this->user);
        $comment = $this->comments->first();
        $data= [
            'body' => 'update',
            'rating' => 4,
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id
        ];
        $response = $this->put(route('comments.update', ['comment' => $comment]), $data);
        $response->assertRedirect(route('photos.show', ['photo' => $comment->photo]));
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedUpdateCommentIfNotAuth()
    {
        $comment = $this->comments->first();
        $data= [
            'body' => 'update',
            'rating' => 4,
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id
        ];
        $response = $this->put(route('comments.update', ['comment' => $comment]), $data);
        $response->assertRedirect(route('login'));
        $this->assertDatabaseMissing('comments', $data);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedUpdateCommentIfAnotherUser()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);
        $comment = $this->comments->first();
        $data= [
            'body' => 'update',
            'rating' => 4,
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id
        ];
        $response = $this->put(route('comments.update', ['comment' => $comment]), $data);
        $response->assertStatus(403);
        $this->assertDatabaseMissing('comments', $data);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedDeleteCommentIfNotAuth()
    {
        $response = $this->delete(route('comments.destroy', ['comment' => $this->comments->first()]));
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedDeleteCommentIfAnotherUser()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);
        $response = $this->delete(route('comments.destroy', ['comment' => $this->comments->first()]));
        $response->assertStatus(403);
    }
}
