<?php

namespace Tests\Feature;

use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;

    private $user, $photos;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->photos = factory(Photo::class, 3)->create([
            'user_id' => $this->user->id
        ]);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testSuccessGetPhotos()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testSuccessShowPhoto()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('photos.show', ['photo' => $this->photos->first()]));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testFailedShowPhotoIfNotAuth()
    {
        $response = $this->get(route('photos.show', ['photo' => $this->photos->first()]));
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testFailedStorePhotoIfNotAuth()
    {
        $photo = [
            'user_id' => $this->user->id,
            'title' => 'Test title',
            'picture' => 'test picture'
        ];
        $response = $this->post(route('photos.store', $photo));
        $response->assertRedirect(route('login'));
        $this->assertDatabaseMissing('photos', $photo);
    }
}
