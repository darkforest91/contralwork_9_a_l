<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'user_id' => rand(1,3),
        'photo_id' => rand(1, 30),
        'body' => $faker->text,
        'rating' => rand(1, 5)
    ];
});
